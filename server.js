const express = require('express');
const path = require('path');
const logger = require('morgan');
const compression = require('compression');
const methodOverride = require('method-override');
const session = require('express-session');
const flash = require('express-flash');
const bodyParser = require('body-parser');
const expressValidator = require('express-validator');
const fs = require('fs');

const app = express();

app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  res.header('Access-Control-Allow-Methods', 'PUT, POST, GET, DELETE, OPTIONS');
  next();
});

app.set('port', 4000);
app.use(compression());
app.use(logger('dev'));
app.use(bodyParser.urlencoded({ extended: true , limit: '5mb' }));
app.use(bodyParser.json({limit: '5mb'}));
app.use(expressValidator());
app.use(methodOverride('_method'));
//todo: not safe change secret to something secret
app.use(session({ secret: 'secret', resave: true, saveUninitialized: true }));
app.use(flash());
app.use(express.static(path.join(__dirname, 'build')));

app.get('/', function (req, res) {
  res.send("Backend");
});

app.get('/pdf', function (req, res) {
  res.json([{
    id: 1,
    name: 'fakedoc.pdf',
    account: {},
    createdAt: new Date(),
    numPages: 1,
    path: '/fakeserverpath'
  }]);
});
app.get('/documents/1/download', function (req, res) {
  var file = path.join(__dirname, '/pdf/test2.pdf');

  res.download(file, function (err) {
      if (err) {
          console.log("Error");
          console.log(err);
      } else {
          console.log("Success");
      }
  });
});
app.get('/documents/1/entities', function (req, res) {
  res.json([{
    label: "test",
    startY: 5,
    height: 100,
    endX: 5,
    startX: 5,
    id: 1
  }]);
  
});

app.get('/fakeserverpath', (req, res) => {
  var filePath = "/pdf/test2.pdf";

  fs.readFile(__dirname + filePath , function (err,data){
      res.contentType("application/pdf");
      res.send([data]);
  });
})

app.listen(app.get('port'), function() {
  console.log('Express server listening on port ' + app.get('port'));
});

module.exports = app;
